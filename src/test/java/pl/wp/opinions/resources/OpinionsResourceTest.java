package pl.wp.opinions.resources;

import org.glassfish.jersey.client.JerseyInvocation;
import org.junit.Before;
import org.junit.Test;
import pl.wp.opinions.configuration.EndPoint;
import pl.wp.opinions.configuration.EventsLogFactory;
import pl.wp.opinions.configuration.OpinionsConfiguration;
import pl.wp.opinions.configuration.logging.EventLog;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.Collections;

import static javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;
import static javax.ws.rs.core.Response.Status.OK;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Fail.fail;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class OpinionsResourceTest {

    private OpinionsCache opinionsCache = mock(OpinionsCache.class);
    private OpinionsConfiguration configuration = mock(OpinionsConfiguration.class);
    private UriInfo uriInfo = mock(UriInfo.class);
    private HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
    private final Client client = mock(Client.class);
    private final WebTarget target = mock(WebTarget.class);
    private final JerseyInvocation.Builder builder = mock(JerseyInvocation.Builder.class);
    private final Response middlewareResponse = mock(Response.class);
    private EventLog logger = mock(EventLog.class);

    @Before
    public void setUp() {
        final EventsLogFactory eventsLogFactory = mock(EventsLogFactory.class);
        when(configuration.getEventsLogFactory()).thenReturn(eventsLogFactory);
        when(eventsLogFactory.build()).thenReturn(logger);

        when(uriInfo.getPath()).thenReturn("/wp/test");
        when(httpServletRequest.getHeader(anyString())).thenReturn("1.2.3.4");

        final EndPoint endpoint = new EndPoint();
        endpoint.setHost("test_host");
        endpoint.setPort(1234);
        endpoint.setPath("/wp/flake");

        when(configuration.getOpinionsMiddlewareEndPoint()).thenReturn(endpoint);
        when(configuration.getModeratorEndPoint()).thenReturn(endpoint);
        when(configuration.getClient()).thenReturn(client);
        when(client.target((URI) anyObject())).thenReturn(target);
        when(target.path(anyString())).thenReturn(target);
        when(target.request(MediaType.APPLICATION_JSON)).thenReturn(builder);
        when(builder.accept(anyString())).thenReturn(builder);
        when(builder.header(anyString(), anyObject())).thenReturn(builder);
        when(builder.post(anyObject())).thenReturn(middlewareResponse);
        when(httpServletRequest.getRemoteAddr()).thenReturn("10.10.10.10");
        when(httpServletRequest.getHeaderNames()).thenReturn(Collections.emptyEnumeration());
    }

    @Test
    public void shouldReturnOKResponseForDownVotingOpinion() {
        final Response actual = new OpinionsResource(opinionsCache, configuration).downVoteOpinion("test", uriInfo);

        assertThat(actual.getStatus()).isEqualTo(OK.getStatusCode());
    }

    @Test
    public void shouldReturnOKResponseForReportingOpinion() {
        // given
        when(middlewareResponse.getStatusInfo()).thenReturn(OK);
        when(middlewareResponse.getStatus()).thenReturn(200);

        // when
        final Response actual = new OpinionsResource(opinionsCache, configuration).reportOpinion("wp", "11", uriInfo, httpServletRequest);

        // then
        assertThat(actual.getStatus()).isEqualTo(OK.getStatusCode());
        verify(logger, atLeast(1)).reported("11", "wp");
    }

    @Test
    public void shouldFailForReportingOpinion() {
        // given
        when(middlewareResponse.getStatusInfo()).thenReturn(INTERNAL_SERVER_ERROR);
        when(middlewareResponse.getStatus()).thenReturn(500);

        // when
        try {
            new OpinionsResource(opinionsCache, configuration).reportOpinion("wp", "11", uriInfo, httpServletRequest);
            fail("ReportOpinion should throw exception.");
        } catch (Exception actual) {
            assertThat(actual).isExactlyInstanceOf(RuntimeException.class);
            verify(logger, never()).reported(anyString(), anyString());
        }
    }
}