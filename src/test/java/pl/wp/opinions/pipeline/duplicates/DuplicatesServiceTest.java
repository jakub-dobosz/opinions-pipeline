package pl.wp.opinions.pipeline.duplicates;

import com.google.common.hash.HashCode;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import pl.wp.opinions.domain.model.Post;
import pl.wp.opinions.domain.model.TestPosts;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DuplicatesServiceTest {

    private static final Post POST = TestPosts.postWithLongText();
    private static final HashCode HASH = HashCode.fromString("1111222233334444");
    private static final int HASH_COUNT_THRESHOLD = 5;

    @Mock
    private HashInputFactory hashInputFactory;
    @Mock
    private PostHashService posthashService;
    @Mock
    private DuplicatesCache duplicatesCache;

    private DuplicatesService objectUnderTest;

    @Before
    public void setUp() {
        when(hashInputFactory.getFromPost(POST)).thenReturn(POST.getText());
        when(posthashService.getHash(POST)).thenReturn(HASH);
    }

    @Test
    public void postShouldNotBeDuplicatedIfCacheCounterValueIsLessThanHashCountThreshold() {
        int cacheCounterValue = 1;
        when(duplicatesCache.getCounterValue(HASH)).thenReturn(cacheCounterValue);

        objectUnderTest = getDuplicateService();
        boolean actual = objectUnderTest.isDuplicated(POST);

        assertThat(cacheCounterValue).isLessThan(HASH_COUNT_THRESHOLD);
        assertThat(actual).isFalse();
    }

    @Test
    public void postShouldNotBeDuplicatedIfCacheCounterValueIsEqualToHashCountThreshold() {
        int cacheCounterValue = HASH_COUNT_THRESHOLD;
        when(duplicatesCache.getCounterValue(HASH)).thenReturn(cacheCounterValue);

        objectUnderTest = getDuplicateService();
        boolean actual = objectUnderTest.isDuplicated(POST);

        assertThat(cacheCounterValue).isEqualTo(HASH_COUNT_THRESHOLD);
        assertThat(actual).isFalse();
    }

    @Test
    public void postShouldBeDuplicatedIfCacheCounterValueIsGreaterThanHashCountThreshold() {
        int cacheCounterValue = 6;
        when(duplicatesCache.getCounterValue(HASH)).thenReturn(cacheCounterValue);

        objectUnderTest = getDuplicateService();
        boolean actual = objectUnderTest.isDuplicated(POST);

        assertThat(cacheCounterValue).isGreaterThan(HASH_COUNT_THRESHOLD);
        assertThat(actual).isTrue();
    }

    private DuplicatesService getDuplicateService() {
        return new DuplicatesService(posthashService, duplicatesCache, HASH_COUNT_THRESHOLD);
    }
}
