package pl.wp.opinions.pipeline.duplicates;

import com.google.common.hash.HashCode;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DuplicatesCacheTest {

    private static final int MAXIMUM_CACHE_SIZE = 1000000;
    private static final HashCode HASH = HashCode.fromString("1111222233334444");

    private DuplicatesCache objectUnderTest;

    @Before
    public void setUp() {
        objectUnderTest = new DuplicatesCache(MAXIMUM_CACHE_SIZE);
    }

    @Test
    public void counterValueShouldBeEqualToOneIfThereIsNoHashInCache() {
        Integer counterValue = getCounterValue();

        assertThat(counterValue).isEqualTo(1);
    }

    @Test
    public void shouldIncreaseCounterValueByOneIfHashExistsInCache() {
        Integer previousCounterValue = getCounterValue();

        Integer counterValue = getCounterValue();

        assertThat(counterValue).isEqualTo(previousCounterValue + 1);
    }

    private Integer getCounterValue() {
        return objectUnderTest.getCounterValue(HASH);
    }
}
