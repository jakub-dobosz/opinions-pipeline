package pl.wp.opinions.pipeline.duplicates;

import org.junit.Test;
import pl.wp.opinions.domain.model.Post;
import pl.wp.opinions.domain.model.TestPosts;

import static org.assertj.core.api.Assertions.assertThat;

public class HashInputFactoryTest {

    private static final int POST_TEXT_LENGTH_THRESHOLD = 15;
    private static final Post POST_WITH_SHORT_TEXT = TestPosts.postWithShortText();
    private static final Post POST_WITH_MEDIUM_TEXT = TestPosts.postWithMediumText();
    private static final Post POST_WITH_LONG_TEXT = TestPosts.postWithLongText();

    @Test
    public void hashInputShouldContainPostTextAndIpAddressAndUserNameWhenShortPostGiven() {
        String postText = POST_WITH_SHORT_TEXT.getText();
        String ipAddress = POST_WITH_SHORT_TEXT.getUserData().getIpAddress();
        String userName = POST_WITH_SHORT_TEXT.getUserData().getUserName();

        String hashInput = getHashInputFactory().getFromPost(POST_WITH_SHORT_TEXT);

        assertThat(hashInput).contains(postText, ipAddress, userName);
    }

    @Test
    public void hashInputLengthShouldBeEqualToSumOfPostTextIpAndUserWhenShortPostGiven() {
        int sumOfPostTextIpAndUserLengths =
                POST_WITH_SHORT_TEXT.getText().length() +
                POST_WITH_SHORT_TEXT.getUserData().getIpAddress().length() +
                POST_WITH_SHORT_TEXT.getUserData().getUserName().length();

        int hashInputLength = getHashInputFactory().getFromPost(POST_WITH_SHORT_TEXT).length();

        assertThat(hashInputLength).isEqualTo(sumOfPostTextIpAndUserLengths);
    }

    @Test
    public void hashInputLengthShouldBeEqualToPostTextWhenNormalPostGiven() {
        int postTextLength = POST_WITH_MEDIUM_TEXT.getText().length();

        int hashInputLength = getHashInputFactory().getFromPost(POST_WITH_MEDIUM_TEXT).length();

        assertThat(hashInputLength).isEqualTo(postTextLength);
    }

    @Test
    public void hashInputLengthShouldBeShorterThenPostTextWhenLongPostGiven() {
        int postTextLength = POST_WITH_LONG_TEXT.getText().length();

        int hashInputLength = getHashInputFactory().getFromPost(POST_WITH_LONG_TEXT).length();

        assertThat(hashInputLength).isLessThan(postTextLength);
    }

    private HashInputFactory getHashInputFactory() {
        return new HashInputFactory(POST_TEXT_LENGTH_THRESHOLD);
    }
}
