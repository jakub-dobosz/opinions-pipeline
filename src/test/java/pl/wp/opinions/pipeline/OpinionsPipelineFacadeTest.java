package pl.wp.opinions.pipeline;

import com.google.common.collect.Sets;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import pl.wp.opinions.configuration.logging.EventLog;
import pl.wp.opinions.domain.model.Post;
import pl.wp.opinions.domain.model.TestPosts;
import pl.wp.opinions.pipeline.duplicates.DuplicatesService;

import java.util.Set;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OpinionsPipelineFacadeTest {

    @Mock
    private OpinionsPipelineCommandFactory opinionsPipelineCommandFactory;
    @Mock
    private DuplicatesService duplicatesService;
    @Mock
    private EventLog eventLog;
    @Mock
    private OpinionsPipelineCommand opinionsPipelineCommand;

    @InjectMocks
    private OpinionsPipelineFacade objectUnderTest;

    private final Set<Post> shortAndMediumAndLongPosts = TestPosts.shortAndMediumAndLongPosts();
    private final Set<Post> shortAndMediumPosts = TestPosts.shortAndMediumPosts();
    private final Set<Post> duplicatedPosts = Sets.newHashSet(TestPosts.postWithLongText());
    private final Set<Post> emptySet = Sets.newHashSet();

    @Test
    public void shouldLogWhenDuplicatedPostExists() {
        when(duplicatesService.getUniquePosts(shortAndMediumAndLongPosts)).thenReturn(shortAndMediumPosts);
        when(opinionsPipelineCommandFactory.create(shortAndMediumPosts)).thenReturn(opinionsPipelineCommand);

        objectUnderTest.process(shortAndMediumAndLongPosts);

        verify(eventLog, times(1)).duplicated(duplicatedPosts);
    }

    @Test
    public void shouldNotCreateCommandWhenEmptySetOfPostsReturnedFromDuplicatesService() {
        when(duplicatesService.getUniquePosts(shortAndMediumPosts)).thenReturn(emptySet);

        objectUnderTest.process(shortAndMediumPosts);

        verify(opinionsPipelineCommandFactory, never()).create(emptySet);
    }
}
