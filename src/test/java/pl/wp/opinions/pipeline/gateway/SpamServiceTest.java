package pl.wp.opinions.pipeline.gateway;

import org.glassfish.jersey.client.JerseyInvocation;
import org.junit.Before;
import org.junit.Test;
import pl.wp.opinions.UnSuccessfulResponseException;
import pl.wp.opinions.domain.model.SpamLevel;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SpamServiceTest {

    private final static String MESSAGE = "text";

    private final WebTarget target = mock(WebTarget.class);
    private final JerseyInvocation.Builder builder = mock(JerseyInvocation.Builder.class);
    private final Response response = mock(Response.class);

    private SpamService objectUnderTest;

    @Before
    public void setUp() {
        when(target.request(MediaType.TEXT_PLAIN)).thenReturn(builder);
        when(builder.post(anyObject())).thenReturn(response);

        objectUnderTest = new SpamService(target);
    }

    @Test
    public void shouldReturnSpamStatus() {
        stubSpamResponse();

        final SpamLevel actual = objectUnderTest.test(MESSAGE);

        assertThat(actual).isEqualTo(SpamLevel.SPAM);
    }

    private void stubSpamResponse() {
        stubValidResponse(true);
    }

    private void stubValidResponse(boolean isSpam) {
        when(response.getStatus()).thenReturn(200);
        when(response.readEntity(Boolean.class)).thenReturn(isSpam);
    }

    @Test
    public void shouldReturnNotSpamStatus() {
        stubNotSpamResponse();

        final SpamLevel actual = objectUnderTest.test(MESSAGE);

        assertThat(actual).isEqualTo(SpamLevel.NOT_SPAM);
    }

    private void stubNotSpamResponse() {
        stubValidResponse(false);
    }

    @Test(expected = UnSuccessfulResponseException.class)
    public void shouldReturnExceptionOnServerError() {
        when(response.getStatus()).thenReturn(500);

        objectUnderTest.test(MESSAGE);
    }

    @Test
    public void shouldReturnErrorAsDefaultSpamLevel() {
        final SpamLevel actual = objectUnderTest.getDefault();

        assertThat(actual).isEqualTo(SpamLevel.ERROR);
    }
}
