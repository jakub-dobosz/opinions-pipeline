package pl.wp.opinions.pipeline.gateway;

import org.junit.Before;
import org.junit.Test;
import pl.wp.opinions.UnSuccessfulResponseException;

import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class WpFlakeServiceTest {

    private final WebTarget target = mock(WebTarget.class);
    private final Invocation.Builder builder = mock(Invocation.Builder.class);
    private final Response clientResponse = mock(Response.class);

    @Before
    public void setUp() throws Exception {
        when(target.request(MediaType.TEXT_PLAIN_TYPE)).thenReturn(builder);
        when(builder.get()).thenReturn(clientResponse);
    }

    @Test
    public void shouldReturnRandomIDWhenClientResponseIsOK() {
        String expectedId = "123345";
        when(clientResponse.getStatus()).thenReturn(200);
        when(clientResponse.readEntity(String.class)).thenReturn(expectedId);

        final String actual = new WpFlakeService(target).getId();

        assertThat(actual).isEqualTo(expectedId);
    }

    @Test(expected = UnSuccessfulResponseException.class)
    public void shouldThrowUnSuccessfulResponseExceptionWhenClientResponseIsNotOK() {
        when(clientResponse.getStatus()).thenReturn(503);

        new WpFlakeService(target).getId();
    }
}