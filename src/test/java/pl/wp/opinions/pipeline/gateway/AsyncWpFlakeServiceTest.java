package pl.wp.opinions.pipeline.gateway;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import pl.wp.opinions.UnSuccessfulResponseException;
import rx.Single;
import rx.observers.TestSubscriber;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AsyncWpFlakeServiceTest {

    @Mock
    private WpFlakeService wpFlakeService;

    @InjectMocks
    private AsyncWpFlakeService objectUnderTest;

    @Test
    public void shouldThrowPostIdNotSetExceptionWhenWpFlakeServiceThrowsUnsuccessfulResponseException() {
        String message = "WplFlake zwrócił niepoprawny status";
        when(wpFlakeService.getId()).thenThrow(new UnSuccessfulResponseException(message));
        TestSubscriber<String> subscriber = new TestSubscriber<>();

        Single<String> id = objectUnderTest.getId();
        id.subscribe(subscriber);

        subscriber.assertError(PostIdNotSetException.class);
    }
}
