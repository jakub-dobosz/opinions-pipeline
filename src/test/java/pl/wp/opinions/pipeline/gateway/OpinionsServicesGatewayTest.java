package pl.wp.opinions.pipeline.gateway;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import pl.wp.opinions.domain.model.Post;
import pl.wp.opinions.domain.model.SpamLevel;
import pl.wp.opinions.domain.model.TestPosts;
import rx.Single;
import rx.observers.TestSubscriber;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OpinionsServicesGatewayTest {

    @Mock
    private AsyncWpFlakeService wpFlakeService;
    @Mock
    private AsyncTestService<Boolean> censorService;
    @Mock
    private AsyncTestService<SpamLevel> spamService;

    @InjectMocks
    private OpinionsServicesGateway objectUnderTest;

    @Test
    public void shouldThrowPostIdNotSetExceptionWhenWpFlakeFails() {
        when(wpFlakeService.getId()).thenReturn(Single.error(new PostIdNotSetException()));
        Post post = TestPosts.postWithShortText();
        TestSubscriber<Post> testSubscriber = new TestSubscriber<>();

        objectUnderTest.getProcessedPost(post).subscribe(testSubscriber);

        testSubscriber.assertError(PostIdNotSetException.class);
    }
}
