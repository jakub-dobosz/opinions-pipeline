package pl.wp.opinions.pipeline.gateway;

import org.junit.Before;
import org.junit.Test;
import pl.wp.opinions.UnSuccessfulResponseException;

import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;
import static javax.ws.rs.core.Response.Status.OK;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CensorServiceTest {

    private final WebTarget target = mock(WebTarget.class);
    private final Invocation.Builder builder = mock(Invocation.Builder.class);
    private final Response clientResponse = mock(Response.class);

    private CensorService objectUnderTest;

    @Before
    public void setUp() throws Exception {
        when(target.path(anyString())).thenReturn(target);
        when(target.request(MediaType.TEXT_PLAIN_TYPE)).thenReturn(builder);
        when(builder.post(anyObject())).thenReturn(clientResponse);

        objectUnderTest = new CensorService(target);
    }

    @Test(expected = UnSuccessfulResponseException.class)
    public void shouldThrowUnSuccessfulResponseExceptionWhenClientResponseIsNotOK() {
        when(clientResponse.getStatusInfo()).thenReturn(INTERNAL_SERVER_ERROR);

        objectUnderTest.test("asd");
    }

    @Test
    public void shouldReturnTrueWhenClientResponseBadRequest() {
        when(clientResponse.getStatus()).thenReturn(400);
        when(clientResponse.getStatusInfo()).thenReturn(BAD_REQUEST);

        final boolean actual = objectUnderTest.test("asd");

        assertThat(actual).isTrue();
    }

    @Test
    public void shouldReturnFalseWhenClientResponseOk() {
        when(clientResponse.getStatus()).thenReturn(200);
        when(clientResponse.getStatusInfo()).thenReturn(OK);

        final boolean actual = objectUnderTest.test("asd");

        assertThat(actual).isFalse();
    }

    @Test
    public void shouldReturnFalseAsDefaultCensorResponse() {
        final boolean actual = objectUnderTest.getDefault();

        assertThat(actual).isFalse();
    }
}
