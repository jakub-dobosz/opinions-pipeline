package pl.wp.opinions.pipeline.gateway;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import pl.wp.opinions.UnSuccessfulResponseException;
import rx.Single;
import rx.observers.TestSubscriber;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SuppressWarnings("unchecked")
@RunWith(MockitoJUnitRunner.class)
public class AsyncTestServiceTest {

    @Mock
    private TestService<String> testService;

    @InjectMocks
    private AsyncTestService<String> objectUnderTest;

    @Test
    public void shouldReturnDefaultTestServiceValueWhenTestServiceThrowsException() {
        final String message = "test message";
        when(testService.test(message)).thenThrow(new UnSuccessfulResponseException("error message"));
        final String expected = "default response";
        when(testService.getDefault()).thenReturn(expected);
        TestSubscriber<String> subscriber = new TestSubscriber<>();

        Single<String> id = objectUnderTest.test(message);
        id.subscribe(subscriber);

        subscriber.assertValue(expected);
        verify(testService, times(1)).getDefault();
    }
}
