package pl.wp.opinions.pipeline;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import pl.wp.opinions.domain.model.Post;
import pl.wp.opinions.domain.model.TestPosts;
import pl.wp.opinions.pipeline.gateway.OpinionsServicesGateway;
import pl.wp.opinions.pipeline.gateway.PostIdNotSetException;
import pl.wp.opinions.pipeline.middleware.OpinionsMiddlewareService;
import rx.Observable;
import rx.Single;
import rx.observers.TestSubscriber;

import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OpinionsPipelineTest {

    @Mock
    private OpinionsServicesGateway opinionsServicesGateway;
    @Mock
    private OpinionsMiddlewareService opinionsMiddlewareService;

    @InjectMocks
    private OpinionsPipeline objectUnderTest;

    @Test
    public void shouldBypassPostWhenOpinionsServicesGatewayReturnsError() {
        final Post shortPost = TestPosts.postWithShortText();
        final Post mediumPost = TestPosts.postWithMediumText();
        final Post longPost = TestPosts.postWithLongText();
        final Set<Post> shortAndMediumAndLongPosts = TestPosts.shortAndMediumAndLongPosts();
        final Set<Post> shortAndMediumPosts = TestPosts.shortAndMediumPosts();

        when(opinionsServicesGateway.getProcessedPost(shortPost)).thenReturn(Single.just(shortPost));
        when(opinionsServicesGateway.getProcessedPost(mediumPost)).thenReturn(Single.just(mediumPost));
        when(opinionsServicesGateway.getProcessedPost(longPost)).thenReturn(Single.error(new PostIdNotSetException()));

        TestSubscriber<Set<Post>> subscriber = new TestSubscriber<>();

        objectUnderTest.process(shortAndMediumAndLongPosts).subscribe(subscriber);

        subscriber.assertValue(shortAndMediumPosts);
    }

    @Test
    public void test() {
        Single<Set<Integer>> single = Observable.just("1", "2", "three", "4", "5")
                .flatMap(this::map)
                .toList()
                .toSingle()
                .map(HashSet::new);

            single.doOnSuccess(set -> System.out.println(set + "asdfasdfasdf"));

            single.doOnSuccess(set -> System.out.println("test1" + set))
                    .doOnSuccess(set -> System.out.println("test2" + set))
                        .subscribe(System.out::println, throwable -> System.out.println("error"));
    }

    private Observable<Integer> map(String text) {
        return Observable.defer(() -> {
            try {
                int someInt = Integer.parseInt(text);
                return Observable.just(someInt);
            } catch (Exception ex) {
                return Observable.error(ex);
            }
        }).onExceptionResumeNext(Observable.empty());
    }
}
