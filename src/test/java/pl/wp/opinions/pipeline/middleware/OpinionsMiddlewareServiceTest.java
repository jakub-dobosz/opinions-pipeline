package pl.wp.opinions.pipeline.middleware;

import org.junit.Before;
import org.junit.Test;
import pl.wp.opinions.domain.model.Post;
import pl.wp.opinions.domain.model.TestPosts;
import pl.wp.opinions.UnSuccessfulResponseException;
import pl.wp.opinions.dto.BulkDto;
import pl.wp.opinions.dto.BulkDtoFactory;

import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import java.util.Set;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class OpinionsMiddlewareServiceTest {

    private WebTarget target = mock(WebTarget.class);
    private BulkDtoFactory bulkDtoFactory = mock(BulkDtoFactory.class);
    private DestinationPathProvider destinationPathProvider = mock(DestinationPathProvider.class);

    private final Invocation.Builder builder = mock(Invocation.Builder.class);
    private final Response response = mock(Response.class);
    private final Set<Post> posts = TestPosts.shortAndMediumPosts();
    private final BulkDto bulkDto = new BulkDto("test", posts);

    private OpinionsMiddlewareService objectUnderTest;

    @Before
    public void setUp() {
        when(bulkDtoFactory.newInstance(posts)).thenReturn(bulkDto);
        when(target.path(anyString())).thenReturn(target);
        when(target.request(anyString())).thenReturn(builder);
        when(builder.accept(anyString())).thenReturn(builder);
        when(builder.header(anyString(), anyString())).thenReturn(builder);
        when(builder.post(any())).thenReturn(response);

        objectUnderTest = new OpinionsMiddlewareService(target, bulkDtoFactory, destinationPathProvider);
    }

    @Test
    public void shouldNotThrowUnSuccessfulResponseExceptionWhenClientResponseOK() {
        mockResponseStatus(Response.Status.OK);

        send(posts);
    }


    @Test
    public void shouldNotThrowUnSuccessfulResponseExceptionWhenClientResponseNoContent() {
        mockResponseStatus(Response.Status.NO_CONTENT);

        send(posts);
    }

    @Test
    public void shouldNotThrowUnSuccessfulResponseExceptionWhenClientResponseCreated() {
        mockResponseStatus(Response.Status.CREATED);

        send(posts);
    }

    @Test
    public void shouldNotThrowUnSuccessfulResponseExceptionWhenClientResponseGoneStatus() {
        mockResponseStatus(Response.Status.GONE);

        send(posts);
    }

    @Test(expected = UnSuccessfulResponseException.class)
    public void shouldThrowUnSuccessfulResponseExceptionWhenClientResponseFromRedirectionFamily() {
        mockResponseStatus(Response.Status.MOVED_PERMANENTLY);

        send(posts);
    }

    @Test(expected = UnSuccessfulResponseException.class)
    public void shouldThrowUnSuccessfulResponseExceptionWhenClientResponseFromClientErrorFamily() {
        mockResponseStatus(Response.Status.FORBIDDEN);

        send(posts);
    }

    @Test(expected = UnSuccessfulResponseException.class)
    public void shouldThrowUnSuccessfulResponseExceptionWhenClientResponseFromServerErrorFamily() {
        mockResponseStatus(Response.Status.INTERNAL_SERVER_ERROR);

        send(posts);
    }

    private void mockResponseStatus(Response.Status responseStatus) {
        when(response.getStatusInfo()).thenReturn(responseStatus);
    }

    private void send(Set<Post> posts) {
        objectUnderTest.send(posts);
    }
}
