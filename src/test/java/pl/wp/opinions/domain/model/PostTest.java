package pl.wp.opinions.domain.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.dropwizard.jackson.Jackson;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.io.IOException;
import java.util.Set;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.Assertions.assertThat;

public class PostTest {

    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    private static Validator validator;

    @BeforeClass
    public static void setUpValidator() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Before
    public void setUp() {
        MAPPER.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }

    @Test
    public void shouldSerializeToJSON() throws IOException {
        // given
        final UserData userData = getUserData();
        final Post post = getOpinion(userData);

        // when & then
        assertThat(MAPPER.writeValueAsString(post)).isEqualTo(fixture("fixtures/opinionWithoutRequestData.json"));
    }

    @Test
    public void shouldDeserializeJSON() throws IOException {
        // given
        final UserData userData = getUserData();
        final Post expected = getOpinion(userData);

        // when & then
        assertThat(MAPPER.readValue(fixture("fixtures/opinion.json"), Post.class)).isEqualTo(expected);
    }

    private Post getOpinion(final UserData userData) {
        return Post.builder().censored(false)
                .userData(userData)
                .text("text opinii")
                .topicUri("wiadomosci.html").build();
    }

    private UserData getUserData() {
        return UserData.builder().ipAddress("1.2.3.4").userName("janusz").build();
    }

    @Test
    public void tooLongMetadata() throws IOException {
        final Post post = Post.builder().censored(false)
                .userData(getUserData())
                .text("text opinii")
                .topicUri("wiadomosci.html")
                .moderationType(ModerationType.POST_MODERATION)
                .metadata(getTooLongMetadata())
                .build();

        Set<ConstraintViolation<Post>> constraintViolations =
                validator.validate(post);

        assertThat(constraintViolations.size()).isEqualTo(1);
        assertThat(constraintViolations.iterator().next().getMessage()).isEqualTo("size must be between 0 and 1000");
    }

    private String getTooLongMetadata() {
        String a = "";
        for (int i = 0; i < 1001; i++) {
            a = a + "a";
        }
        return a;
    }

}