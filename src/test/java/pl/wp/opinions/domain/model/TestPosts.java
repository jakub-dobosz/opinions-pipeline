package pl.wp.opinions.domain.model;

import com.google.common.collect.Sets;

import java.util.Set;

public class TestPosts {

    public static Set<Post> shortAndMediumPosts() {
        return Sets.newHashSet(postWithShortText(), postWithMediumText());
    }

    public static Set<Post> shortAndMediumAndLongPosts() {
        return Sets.newHashSet(postWithShortText(), postWithMediumText(), postWithLongText());
    }

    public static Post postWithShortText() {
        return customPost(
                "no i fajnie",
                "222.222.222.222",
                "bob"
        );
    }

    public static Post postWithMediumText() {
        return customPost(
                "ten news jest interesujacy",
                "222.222.222.222",
                "marek"
        );
    }

    public static Post postWithLongText() {
        return customPost(
                "no i fajnie, żeś tu napisał. Podoba mi się to!",
                "222.222.222.222",
                "jan"
        );
    }

    private static Post customPost(String text, String ip, String user) {
        UserData userData = UserData.builder()
                .ipAddress(ip)
                .userName(user)
                .build();

        return Post.builder()
                .text(text)
                .userData(userData)
                .build();
    }
}
