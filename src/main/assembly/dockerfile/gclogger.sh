#!/bin/sh

function log_stdout {
	ts=`date +"%Y-%m-%d %H:%M:%S"`
	echo "gc_log $ts $1"
}

exec < /tmp/gc_log_pipe

log_stdout "GC LOGGER START local-mode"

while read; do
	log_stdout "$REPLY"
done

log_stdout "GC LOGGER STOP local-mode"

rm -f /tmp/gc_log_pipe
