#!/bin/sh
set -xv

mknod "/tmp/gc_log_pipe" p

bash "$APP_HOME/gclogger.sh" &

sleep 5

JAVA_PATH=/usr/bin/java
DIR=${APP_HOME}

GENERIC_JAVA_OPTS="
-Xms${JVM_XMS:=500m}
-Xmx${JVM_XMX:=500m}
-XX:HeapDumpPath=$APP_HOME/WP/diag/
-XX:+HeapDumpOnOutOfMemoryError
-XX:-PrintConcurrentLocks
-XX:-PrintCommandLineFlags
-XX:+UseCompressedOops
"

GCLOG_JAVA_OPTS="
-XX:+UseParNewGC
-XX:+UseConcMarkSweepGC
-XX:+PrintGCDetails
-XX:+PrintGCDateStamps
-XX:+PrintGCTimeStamps
-XX:-PrintTenuringDistribution
-XX:+DisableExplicitGC
-Xloggc:/tmp/gc_log_pipe
"

JMX_NOAUTH_NOSSL_JAVA_STARTUP_OPTS="
-Dcom.sun.management.jmxremote
-Dcom.sun.management.jmxremote.local.only=false
-Dcom.sun.management.jmxremote.port=${JMX_PORT:=9998}
-Dcom.sun.management.jmxremote.rmi.port=${JMX_PORT:=9998}
-Dcom.sun.management.jmxremote.authenticate=false
-Dcom.sun.management.jmxremote.ssl=false
-Djava.rmi.server.hostname=$HOST_IP
"

echo -n "Starting application: "

exec $JAVA_PATH $GENERIC_JAVA_OPTS $GCLOG_JAVA_OPTS $JMX_NOAUTH_NOSSL_JAVA_STARTUP_OPTS -jar $DIR/opinions-pipeline.jar server $DIR/config.yml
