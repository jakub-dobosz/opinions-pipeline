package pl.wp.opinions;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.SerializationFeature;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import lombok.extern.log4j.Log4j;
import pl.wp.opinions.cache.TopicIdProvider;
import pl.wp.opinions.pipeline.OpinionsPipeline;
import pl.wp.opinions.pipeline.OpinionsPipelineCommand;
import pl.wp.opinions.pipeline.OpinionsPipelineCommandFactory;
import pl.wp.opinions.pipeline.OpinionsPipelineFactory;
import pl.wp.opinions.pipeline.OpinionsPipelineFacade;
import pl.wp.opinions.cache.GuavaOpinionsCache;
import pl.wp.opinions.configuration.HystrixConfiguration;
import pl.wp.opinions.configuration.OpinionsConfiguration;
import pl.wp.opinions.configuration.bundles.CorsBundle;
import pl.wp.opinions.configuration.bundles.HttpClientBundle;
import pl.wp.opinions.configuration.bundles.HystrixBundle;
import pl.wp.opinions.configuration.logging.EventLog;
import pl.wp.opinions.pipeline.duplicates.DuplicatesService;
import pl.wp.opinions.resources.OpinionsCache;
import pl.wp.opinions.resources.OpinionsResource;

@Log4j
public class OpinionsApplication extends Application<OpinionsConfiguration> {

    public static void main(String[] args) throws Exception {
        new OpinionsApplication().run(args);
    }

    @Override
    public String getName() {
        return "OpinionsApplication-pipeline";
    }

    @Override
    public void initialize(Bootstrap<OpinionsConfiguration> bootstrap) {
        bootstrap.getObjectMapper().configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        bootstrap.getObjectMapper().setSerializationInclusion(Include.NON_NULL);
        bootstrap.addBundle(new AssetsBundle("/assets/", "/docs/"));
        bootstrap.addBundle(new CorsBundle());
        bootstrap.addBundle(new HystrixBundle());
        bootstrap.addBundle(new HttpClientBundle());

        bootstrap.setConfigurationSourceProvider(
                new SubstitutingSourceProvider(bootstrap.getConfigurationSourceProvider(),
                        new EnvironmentVariableSubstitutor()
                )
        );
    }

    @Override
    public void run(OpinionsConfiguration configuration, Environment environment) {
        final OpinionsPipelineFactory opinionsPipelineFactory = new OpinionsPipelineFactory(configuration);
        final OpinionsPipeline opinionsPipeline = opinionsPipelineFactory.create();
        final EventLog eventLog = configuration.getEventsLogFactory().build();
        final DuplicatesService duplicatesService = configuration.getDuplicatesServiceFactory().build();
        final HystrixConfiguration hystrixConfiguration = configuration.getHystrix();
        final OpinionsPipelineCommandFactory commandFactory = posts -> new OpinionsPipelineCommand(opinionsPipeline, hystrixConfiguration, eventLog, posts);
        final OpinionsPipelineFacade opinionsPipelineFacade = new OpinionsPipelineFacade(commandFactory, duplicatesService, eventLog);
        final OpinionsCache opinionsCache = new GuavaOpinionsCache(opinionsPipelineFacade, new TopicIdProvider(), configuration.getOpinionsCache().getEvictionIntervalTimerInSeconds());

        final OpinionsResource resource = new OpinionsResource(opinionsCache, configuration);

        environment.jersey().register(resource);
    }
}
