package pl.wp.opinions.configuration;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.spi.AppenderAttachableImpl;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableList;
import io.dropwizard.logging.AppenderFactory;
import io.dropwizard.logging.ConsoleAppenderFactory;
import org.slf4j.LoggerFactory;
import pl.wp.opinions.configuration.logging.EventLog;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class EventsLogFactory {

    private EventLog logger;

    @JsonProperty
    public String prefix;

    @Valid
    @NotNull
    private ImmutableList<AppenderFactory> appenders = ImmutableList.<AppenderFactory>of(
            new ConsoleAppenderFactory()
    );

    @JsonProperty
    public void setAppenders(ImmutableList<AppenderFactory> appenders) {
        this.appenders = appenders;
    }

    public EventLog build() {
        if (this.logger != null) {
            return this.logger;
        }

        final Logger logger = (Logger) LoggerFactory.getLogger("opinions-json-stats-logging");
        logger.setAdditive(false);

        final LoggerContext context = logger.getLoggerContext();

        final AppenderAttachableImpl<ILoggingEvent> attachable = new AppenderAttachableImpl<>();
        for (AppenderFactory output : this.appenders) {
            attachable.addAppender(output.build(context, "opinions", null));
        }

        this.logger = new EventLog(attachable);
        return this.logger;
    }
}
