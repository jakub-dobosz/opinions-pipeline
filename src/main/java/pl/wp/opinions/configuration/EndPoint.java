package pl.wp.opinions.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.validator.constraints.NotEmpty;

import javax.ws.rs.core.UriBuilder;
import java.net.URI;

/**
 * @author krabczak
 */
@Data
@EqualsAndHashCode
@NoArgsConstructor
@ToString
public class EndPoint {

    @NotEmpty
    @JsonProperty
    private String host;

    @JsonProperty
    private int port;

    @NotEmpty
    @JsonProperty
    private String path;

    public URI getURI() {
        UriBuilder uriBuilder = UriBuilder.fromUri(host);
        if (port > 0) {
            uriBuilder = uriBuilder.port(port);
        }
        return uriBuilder.path(path).build();
    }

}
