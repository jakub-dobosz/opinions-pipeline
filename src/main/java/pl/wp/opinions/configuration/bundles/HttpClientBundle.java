package pl.wp.opinions.configuration.bundles;

import io.dropwizard.ConfiguredBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import pl.wp.opinions.configuration.OpinionsConfiguration;

public class HttpClientBundle implements ConfiguredBundle<OpinionsConfiguration> {
    @Override
    public void run(OpinionsConfiguration configuration, Environment environment) throws Exception {
        configuration.initializeHttpClient(environment);
    }

    @Override
    public void initialize(Bootstrap<?> bootstrap) {

    }
}
