package pl.wp.opinions.configuration.bundles;

import com.netflix.hystrix.contrib.codahalemetricspublisher.HystrixCodaHaleMetricsPublisher;
import com.netflix.hystrix.contrib.metrics.eventstream.HystrixMetricsStreamServlet;
import com.netflix.hystrix.strategy.HystrixPlugins;
import io.dropwizard.ConfiguredBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import lombok.extern.log4j.Log4j;
import pl.wp.opinions.configuration.OpinionsConfiguration;

@Log4j
public class HystrixBundle implements ConfiguredBundle<OpinionsConfiguration> {
    @Override
    public void run(OpinionsConfiguration configuration, Environment environment) throws Exception {
        environment.getApplicationContext().addServlet(HystrixMetricsStreamServlet.class, "/hystrix.stream");

        HystrixCodaHaleMetricsPublisher publisher = new HystrixCodaHaleMetricsPublisher(environment.metrics());
        HystrixPlugins.getInstance().registerMetricsPublisher(publisher);
    }

    @Override
    public void initialize(Bootstrap<?> bootstrap) {
        log.info("Initializing Hystrix configuration");
    }
}
