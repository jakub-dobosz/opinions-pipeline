package pl.wp.opinions.configuration;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class OpinionsCacheConfiguration {

    @Min(30)
    @NotNull
    private int evictionIntervalTimerInSeconds;
}
