package pl.wp.opinions.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import pl.wp.opinions.pipeline.duplicates.DuplicatesCache;
import pl.wp.opinions.pipeline.duplicates.DuplicatesService;
import pl.wp.opinions.pipeline.duplicates.HashInputFactory;
import pl.wp.opinions.pipeline.duplicates.PostHashService;

import javax.validation.constraints.Min;

@Data
public class DuplicatesServiceFactory {

    @Min(1)
    @JsonProperty
    private int postTextLengthThreshold;

    @Min(1)
    @JsonProperty
    private int hashCountThreshold;

    @Min(100)
    @JsonProperty
    private int maximumCacheSize;

    private DuplicatesService duplicatesService;

    public DuplicatesService build() {
        if (duplicatesService != null) {
            return duplicatesService;
        }

        final HashInputFactory hashInputFactory = new HashInputFactory(postTextLengthThreshold);
        final PostHashService postHashService = new PostHashService(hashInputFactory);
        final DuplicatesCache duplicatesCache = new DuplicatesCache(maximumCacheSize);

        this.duplicatesService = new DuplicatesService(postHashService, duplicatesCache, hashCountThreshold);
        return duplicatesService;
    }
}
