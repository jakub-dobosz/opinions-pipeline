package pl.wp.opinions.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class HystrixConfiguration {

    @Min(10)
    @NotNull
    @JsonProperty
    private int coreThreadPoolSize;

    @Min(-1)
    @NotNull
    @JsonProperty
    private int maxQueueSize;
}
