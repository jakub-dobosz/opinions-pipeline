package pl.wp.opinions.configuration.logging;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.core.spi.AppenderAttachableImpl;
import lombok.extern.log4j.Log4j;
import pl.wp.opinions.domain.model.Post;
import pl.wp.opinions.statistics.boundary.EventLogging;
import pl.wp.opinions.statistics.entity.JsonLogEntityBuilder;

import java.util.Collection;

import static pl.wp.opinions.domain.model.SpamLevel.SPAM;

@Log4j
public class EventLog {

    private final AppenderAttachableImpl<ILoggingEvent> appenders;

    private static final String LOGGER_NAME = "opinions-json-stats-logging";

    public EventLog(AppenderAttachableImpl<ILoggingEvent> appenders) {
        this.appenders = appenders;
    }

    public void rejectedByES(Collection<Post> posts) {
        posts.forEach(this::rejectedByES);
    }

    public void rejectedByES(Post post) {
        if (post.getSpamLevel().equals(SPAM)) {
            log(EventLogging.rejectedByEsJson(getLogEntityBuilder(post)));
        }
    }

    private JsonLogEntityBuilder getLogEntityBuilder(Post post) {
        return new JsonLogEntityBuilder().body(post.getText())
                .context(post.getTopicContext())
                .nick(post.getUserData().getUserName())
                .userID(post.getUserData().getUserID())
                .ip(post.getUserData().getIpAddress())
                .isCensored(post.isCensored())
                .moderationType(post.getModerationType().getLegacyCode())
                .objectId(post.getTopicObjectId())
                .objectType(post.getTopicObjectType())
                .postId(post.getId());
    }

    public void duplicated(Collection<Post> posts) {
        posts.forEach(this::duplicated);
    }

    public void duplicated(Post post) {
        log(EventLogging.duplicated(getLogEntityBuilder(post)));
    }

    public void created(Collection<Post> posts) {
        posts.forEach(this::created);
    }

    public void created(Post post) {
        log(EventLogging.created(getLogEntityBuilder(post)));
    }

    public void reported(String id, String context) {
        log(EventLogging.reportedJson(new JsonLogEntityBuilder().context(context).postId(id)));
    }

    private void log(String message) {
        LoggingEvent event = new LoggingEvent();
        event.setLevel(Level.INFO);
        event.setLoggerName(LOGGER_NAME);
        event.setMessage(message);
        event.setTimeStamp(System.currentTimeMillis());
        this.appenders.appendLoopOnAppenders(event);
    }
}
