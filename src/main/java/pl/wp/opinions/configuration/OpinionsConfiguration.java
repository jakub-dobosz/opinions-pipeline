package pl.wp.opinions.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.client.JerseyClientConfiguration;
import io.dropwizard.setup.Environment;
import lombok.Getter;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.ws.rs.client.Client;

@Getter
public class OpinionsConfiguration extends Configuration {

    @Min(1)
    @Max(20)
    @JsonProperty
    private int messageListenersCount;

    @Valid
    @JsonProperty
    private EndPoint censorFilterEndPoint;

    @Valid
    @JsonProperty
    private EndPoint moderatorEndPoint;

    @Valid
    @JsonProperty
    private EndPoint opinionsMiddlewareEndPoint;

    @Valid
    @JsonProperty
    private EndPoint wpFlakeEndPoint;

    @Valid
    @JsonProperty
    private EndPoint spamFilterEndPoint;

    @Valid
    @JsonProperty
    private EndPoint duplicatesFilterEndPoint;

    @Valid
    @NotNull
    private EventsLogFactory eventsLogFactory = new EventsLogFactory();

    @Valid
    @NotNull
    @JsonProperty
    private JerseyClientConfiguration httpClient = new JerseyClientConfiguration();

    @Valid
    @NotNull
    private DuplicatesServiceFactory duplicatesServiceFactory = new DuplicatesServiceFactory();

    @Valid
    @JsonProperty
    private HystrixConfiguration hystrix;

    @Valid
    @JsonProperty
    private OpinionsCacheConfiguration opinionsCache;

    private Client client;

    public void initializeHttpClient(Environment environment) {
        client = new JerseyClientBuilder(environment).using(httpClient).build(environment.getName());
    }

    public Client getClient() {
        return client;
    }
}
