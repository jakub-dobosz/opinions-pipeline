package pl.wp.opinions.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import pl.wp.opinions.domain.model.Post;

import java.io.Serializable;
import java.util.Set;

@Data
@AllArgsConstructor
public class BulkDto implements Serializable {

    private String topicUri;
    private Set<Post> posts;
}
