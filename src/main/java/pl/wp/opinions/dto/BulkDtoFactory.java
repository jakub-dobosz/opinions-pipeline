package pl.wp.opinions.dto;

import pl.wp.opinions.domain.model.Post;

import java.util.Set;

public class BulkDtoFactory {

    public BulkDto newInstance(final Set<Post> posts) {
        String topicUri = getTopicUri(posts);
        return new BulkDto(topicUri, posts);
    }

    @SuppressWarnings("ConstantConditions")
    private String getTopicUri(final Set<Post> posts) {
        return posts.iterator().next().getTopicUri();
    }
}
