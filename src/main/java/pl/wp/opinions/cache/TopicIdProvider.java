package pl.wp.opinions.cache;

import com.google.common.base.Joiner;
import pl.wp.opinions.domain.model.Post;

public class TopicIdProvider {

    private static final char TOPIC_ID_PARTS_SEPARATOR = ':';

    public String get(Post post) {
        return Joiner.on(TOPIC_ID_PARTS_SEPARATOR).join(post.getTopicContext(), post.getTopicObjectType(), post.getTopicObjectId());
    }
}
