package pl.wp.opinions.cache;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalListener;
import com.google.common.collect.Sets;
import pl.wp.opinions.domain.model.Post;
import pl.wp.opinions.resources.OpinionsCache;

import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.BiFunction;

public class GuavaOpinionsCache implements OpinionsCache {

    private final Cache<String, Set<Post>> cache;
    private final TopicIdProvider topicIdProvider;

    public GuavaOpinionsCache(final RemovalListener<String, Set<Post>> removalListener,
                              final TopicIdProvider topicIdProvider,
                              final int evictionIntervalTimerInSeconds) {
        this.cache = buildCache(removalListener, evictionIntervalTimerInSeconds);
        this.topicIdProvider = topicIdProvider;
    }

    private Cache<String, Set<Post>> buildCache(final RemovalListener<String, Set<Post>> removalListener,
                                                final int evictionIntervalTimerInSeconds) {
        return CacheBuilder.newBuilder()
                .expireAfterWrite(evictionIntervalTimerInSeconds, TimeUnit.SECONDS)
                .removalListener(removalListener)
                .build();
    }

    @Override
    public void put(final Post post) {
        String topicId = topicIdProvider.get(post);
        cache.asMap().merge(topicId, Sets.newHashSet(post), setsJoinerFunction());
    }

    private BiFunction<Set<Post>, Set<Post>, Set<Post>> setsJoinerFunction() {
        return (set, unitSet) -> {
            set.addAll(unitSet);
            return set;
        };
    }
}
