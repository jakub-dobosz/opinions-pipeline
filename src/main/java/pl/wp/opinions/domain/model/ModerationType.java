package pl.wp.opinions.domain.model;

public enum ModerationType {
    PRE_MODERATION(0), POST_MODERATION(1);

    private final int legacyCode;

    ModerationType(int legacyCode) {
        this.legacyCode = legacyCode;
    }

    public int getLegacyCode() {
        return legacyCode;
    }
}
