package pl.wp.opinions.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserData implements Serializable {

    public static final long serialVersionUID = 1L;

    @NotBlank
    private String userName;

    private String userID;

    @NotBlank
    private String ipAddress;

}
