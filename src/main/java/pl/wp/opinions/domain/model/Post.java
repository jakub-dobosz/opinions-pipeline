package pl.wp.opinions.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.joda.time.DateTime;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author krabczak
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Post implements Serializable {

    public static final long serialVersionUID = 1L;

    private String id = "0";

    private DateTime created = DateTime.now();

    @NotBlank
    @Length(max = 4000)
    private String text;

    @NotNull
    @Valid
    private UserData userData;

    @Size(max = 1000)
    private String metadata;

    @NotBlank
    private String topicUri;

    @JsonIgnore
    private String topicContext;

    @JsonIgnore
    private String topicObjectType;

    @JsonIgnore
    private String topicObjectId;

    @JsonIgnore
    private String destinationPath;

    private SpamLevel spamLevel;

    private boolean censored;

    @NotNull
    private ModerationType moderationType;

    private String parentPath;
}
