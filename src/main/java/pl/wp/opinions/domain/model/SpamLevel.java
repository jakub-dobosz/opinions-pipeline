package pl.wp.opinions.domain.model;

public enum SpamLevel {
    SPAM, ERROR, NOT_SPAM
}
