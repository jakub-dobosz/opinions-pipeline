package pl.wp.opinions.router;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import lombok.Cleanup;
import pl.wp.opinions.UnSuccessfulResponseException;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.Response.Status.Family.SUCCESSFUL;
import static javax.ws.rs.core.Response.Status.GONE;

abstract class AbstractRouter extends HystrixCommand<Void> {

    private final WebTarget target;

    AbstractRouter(final WebTarget target, HystrixCommandGroupKey hystrixCommandGroupKey) {
        super(hystrixCommandGroupKey);
        this.target = target;
    }

    protected abstract String getRoute();

    void send(final Object object) {
        @Cleanup Response response = target.request(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .header("x-source", "OpinionsRestAPI")
                .header("x-destination", getRoute())
                .post(Entity.json(object));

        if (!response.getStatusInfo().getFamily().equals(SUCCESSFUL) && !response.getStatusInfo().equals(GONE)) {
            throw new UnSuccessfulResponseException("CMS Message Broker zwrócił niepoprawny status : " +
                    response.getStatus() + ", message : " +
                    response.readEntity(String.class)
            );
        }
    }
}
