package pl.wp.opinions.router;

import com.netflix.hystrix.HystrixCommandGroupKey;
import pl.wp.opinions.configuration.OpinionsConfiguration;

public class ModeratorRoute extends AbstractRouter {

    private static final String ROUTE = "ModeratorReportManagement";
    private final String id;

    public ModeratorRoute(final OpinionsConfiguration configuration, String id, String path) {
        super(configuration.getClient().target(configuration.getModeratorEndPoint().getURI()).path(path)
                , HystrixCommandGroupKey.Factory.asKey("ModeratorRoute"));
        this.id = id;
    }

    @Override
    protected String getRoute() {
        return ROUTE;
    }

    @Override
    protected Void run() throws Exception {
        send(id);
        return null;
    }
}
