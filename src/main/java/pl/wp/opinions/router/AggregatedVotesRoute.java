package pl.wp.opinions.router;

import com.google.common.base.Joiner;
import com.netflix.hystrix.HystrixCommandGroupKey;
import pl.wp.opinions.configuration.OpinionsConfiguration;

import java.util.Map;

public class AggregatedVotesRoute extends AbstractRouter {

	private static final String ROUTE = "OpinionsAggregatedVotes";
	private static final String PATH = "resources";
	private final Map<String, Long> payload;

	public AggregatedVotesRoute(final OpinionsConfiguration configuration, Map<String, Long> payload) {
		super(configuration.getClient().target(configuration.getOpinionsMiddlewareEndPoint().getURI()).path(Joiner.on("/").join(PATH, "opinions/aggregated-votes")),
				HystrixCommandGroupKey.Factory.asKey("AggregatedVotesRoute"));
		this.payload = payload;
	}

	@Override
	protected String getRoute() {
		return ROUTE;
	}

	@Override
	protected Void run() throws Exception {
		send(payload);
		return null;
	}
}
