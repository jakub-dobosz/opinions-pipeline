package pl.wp.opinions.pipeline;

import lombok.RequiredArgsConstructor;
import pl.wp.opinions.configuration.OpinionsConfiguration;
import pl.wp.opinions.domain.model.SpamLevel;
import pl.wp.opinions.dto.BulkDtoFactory;
import pl.wp.opinions.pipeline.gateway.AsyncTestService;
import pl.wp.opinions.pipeline.gateway.AsyncWpFlakeService;
import pl.wp.opinions.pipeline.gateway.CensorService;
import pl.wp.opinions.pipeline.gateway.OpinionsServicesGateway;
import pl.wp.opinions.pipeline.gateway.SpamService;
import pl.wp.opinions.pipeline.gateway.WpFlakeService;
import pl.wp.opinions.pipeline.middleware.DestinationPathProvider;
import pl.wp.opinions.pipeline.middleware.OpinionsMiddlewareService;

import javax.ws.rs.client.WebTarget;

@RequiredArgsConstructor
public class OpinionsPipelineFactory {

    private final OpinionsConfiguration opinionsConfiguration;

    public OpinionsPipeline create() {
        final AsyncWpFlakeService asyncWpFlakeService = getAsyncWpFlakeService();
        final AsyncTestService<SpamLevel> asyncSpamService = getAsyncSpamService();
        final AsyncTestService<Boolean> asyncCensorService = getAsyncCensorService();
        final OpinionsServicesGateway gateway = new OpinionsServicesGateway(asyncWpFlakeService, asyncCensorService, asyncSpamService);
        final OpinionsMiddlewareService middlewareService = getOpinionsMiddlewareService();

        return new OpinionsPipeline(gateway, middlewareService);
    }

    private AsyncWpFlakeService getAsyncWpFlakeService() {
        final WpFlakeService wpFlakeService = new WpFlakeService(getWpFlakeWebTarget());
        return new AsyncWpFlakeService(wpFlakeService);
    }

    private WebTarget getWpFlakeWebTarget() {
        return opinionsConfiguration.getClient().target(opinionsConfiguration.getWpFlakeEndPoint().getURI());
    }

    private AsyncTestService<SpamLevel> getAsyncSpamService() {
        final SpamService spamService = new SpamService(getSpamWebTarget());
        return new AsyncTestService<>(spamService);
    }

    private WebTarget getSpamWebTarget() {
        return opinionsConfiguration.getClient().target(opinionsConfiguration.getSpamFilterEndPoint().getURI());
    }

    private AsyncTestService<Boolean> getAsyncCensorService() {
        final CensorService censorService = new CensorService(getCensorWebTarget());
        return new AsyncTestService<>(censorService);
    }

    private WebTarget getCensorWebTarget() {
        return opinionsConfiguration.getClient().target(opinionsConfiguration.getCensorFilterEndPoint().getURI());
    }

    private OpinionsMiddlewareService getOpinionsMiddlewareService() {
        return new OpinionsMiddlewareService(getOpinionsMiddlewareWebTarget(), new BulkDtoFactory(), new DestinationPathProvider());
    }

    private WebTarget getOpinionsMiddlewareWebTarget() {
        return opinionsConfiguration.getClient().target(opinionsConfiguration.getOpinionsMiddlewareEndPoint().getURI());
    }
}
