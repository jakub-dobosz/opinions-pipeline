package pl.wp.opinions.pipeline.gateway;

import rx.Single;

public class AsyncWpFlakeService {

    private final WpFlakeService wpFlakeService;

    public AsyncWpFlakeService(final WpFlakeService wpFlakeService) {
        this.wpFlakeService = wpFlakeService;
    }

    public Single<String> getId() {
        return Single.fromCallable(wpFlakeService::getId).onErrorResumeNext(this::throwPostIdNotSetException);
    }

    private Single<? extends String> throwPostIdNotSetException(Throwable cause) {
        return Single.error(new PostIdNotSetException(cause.getMessage(), cause));
    }
}
