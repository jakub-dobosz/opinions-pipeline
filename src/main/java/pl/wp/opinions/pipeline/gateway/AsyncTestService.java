package pl.wp.opinions.pipeline.gateway;

import rx.Single;

public class AsyncTestService<T> {

    private final TestService<T> testService;

    public AsyncTestService(final TestService<T> testService) {
        this.testService = testService;
    }

    Single<T> test(final String text) {
        return Single.defer(() -> Single.just(testService.test(text))).onErrorResumeNext(getDefaultResponse());
    }

    private Single<T> getDefaultResponse() {
        return Single.just(testService.getDefault());
    }
}
