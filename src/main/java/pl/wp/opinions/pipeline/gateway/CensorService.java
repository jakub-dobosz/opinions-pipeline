package pl.wp.opinions.pipeline.gateway;

import lombok.Cleanup;
import lombok.extern.log4j.Log4j;
import pl.wp.opinions.UnSuccessfulResponseException;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;

@Log4j
public class CensorService implements TestService<Boolean> {

    private final WebTarget target;

    public CensorService(final WebTarget target) {
        this.target = target;
    }

    @Override
    public Boolean test(final String text) {
        @Cleanup Response response = makeRequest(text);
        if (isErroneous(response))
            throw new UnSuccessfulResponseException("CensorService zwrócił HTTP Server Error : " + response.getStatus());
        return readResponse(response);
    }

    private Response makeRequest(final String text) {
        return target.request(MediaType.TEXT_PLAIN_TYPE).post(Entity.text(text));
    }

    private boolean isErroneous(Response response) {
        return response.getStatusInfo().getFamily().equals(Response.Status.Family.SERVER_ERROR);
    }

    private Boolean readResponse(Response response) {
        return response.getStatus() == BAD_REQUEST.getStatusCode();
    }

    @Override
    public Boolean getDefault() {
        return false;
    }
}