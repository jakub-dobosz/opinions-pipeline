package pl.wp.opinions.pipeline.gateway;

import lombok.RequiredArgsConstructor;
import pl.wp.opinions.domain.model.Post;
import pl.wp.opinions.domain.model.SpamLevel;
import rx.Single;

import java.util.concurrent.TimeUnit;

@RequiredArgsConstructor
public class OpinionsServicesGateway {

    private final AsyncWpFlakeService wpFlakeService;
    private final AsyncTestService<Boolean> censorService;
    private final AsyncTestService<SpamLevel> spamService;

    public Single<Post> getProcessedPost(final Post post) {
        return Single.zip(
                wpFlakeService.getId(),
                censorService.test(post.getText()),
                spamService.test(post.getText()),
                (id, isCensored, spamLevel) -> {
                    post.setId(id);
                    post.setCensored(isCensored);
                    post.setSpamLevel(spamLevel);
                    return post;
                })
                .timeout(5000, TimeUnit.MILLISECONDS);
    }
}
