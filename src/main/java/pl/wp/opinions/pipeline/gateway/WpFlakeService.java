package pl.wp.opinions.pipeline.gateway;

import lombok.Cleanup;
import lombok.extern.log4j.Log4j;
import pl.wp.opinions.UnSuccessfulResponseException;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.Response.Status.OK;

@Log4j
public class WpFlakeService {

    private final WebTarget target;

    public WpFlakeService(final WebTarget target) {
        this.target = target;
    }

    String getId() {
        @Cleanup Response response = makeRequest();
        if (isResponseStatusOK(response)) {
            throw new UnSuccessfulResponseException("WpFlake zwrócił niepoprawny status : " + response.getStatus());
        }
        return response.readEntity(String.class);
    }

    private Response makeRequest() {
        return target.request(MediaType.TEXT_PLAIN_TYPE).get();
    }

    private boolean isResponseStatusOK(Response response) {
        return response.getStatus() != OK.getStatusCode();
    }
}