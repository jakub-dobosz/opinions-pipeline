package pl.wp.opinions.pipeline.gateway;

import lombok.Cleanup;
import lombok.extern.log4j.Log4j;
import pl.wp.opinions.domain.model.SpamLevel;
import pl.wp.opinions.UnSuccessfulResponseException;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.Response.Status.OK;

@Log4j
public class SpamService implements TestService<SpamLevel> {

    private final WebTarget target;

    public SpamService(final WebTarget target) {
        this.target = target;
    }

    @Override
    public SpamLevel test(final String text) {
        @Cleanup final Response response = makeRequest(text);
        if (isResponseStatusOK(response)) {
            return readResponse(response);
        } else {
            String message = "Nie udało się przetestować opinii w systemie SpamService";
            log.error(message);
            throw new UnSuccessfulResponseException("SpamService zwrócił błędny HTTP Status : " + response.getStatus());
        }
    }

    private Response makeRequest(final String text) {
        return target.request(MediaType.TEXT_PLAIN).post(Entity.text(text));
    }

    private boolean isResponseStatusOK(Response response) {
        return response.getStatus() == OK.getStatusCode();
    }

    private SpamLevel readResponse(Response response) {
        return response.readEntity(Boolean.class) ? SpamLevel.SPAM : SpamLevel.NOT_SPAM;
    }

    @Override
    public SpamLevel getDefault() {
        return SpamLevel.ERROR;
    }
}