package pl.wp.opinions.pipeline.gateway;

public interface TestService<T> {

    T test(String text);

    T getDefault();
}
