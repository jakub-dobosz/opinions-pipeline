package pl.wp.opinions.pipeline.gateway;

public class PostIdNotSetException extends RuntimeException {
    public static final long serialVersionUID = 1L;

    public PostIdNotSetException() {
    }

    public PostIdNotSetException(String message, Throwable cause) {
        super(message, cause);
    }
}
