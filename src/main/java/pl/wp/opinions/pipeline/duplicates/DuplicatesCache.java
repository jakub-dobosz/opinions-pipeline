package pl.wp.opinions.pipeline.duplicates;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.hash.HashCode;
import lombok.extern.log4j.Log4j;

import java.util.concurrent.TimeUnit;

@Log4j
public class DuplicatesCache {

    private final Cache<HashCode, Integer> cache;

    public DuplicatesCache(int maximumCacheSize) {
        this.cache = buildCache(maximumCacheSize);
    }

    private Cache<HashCode, Integer> buildCache(int maximumCacheSize) {
        return CacheBuilder.newBuilder()
                .maximumSize(maximumCacheSize)
                .expireAfterWrite(24, TimeUnit.HOURS)
                .build();
    }

    public Integer getCounterValue(HashCode hash) {
        Integer counterValue = cache.asMap().merge(hash, 1, (oldValue, value) -> oldValue + value);
        log.debug("Duplicates cache: current counter value for " + hash + " is " + counterValue);
        return counterValue;
    }
}
