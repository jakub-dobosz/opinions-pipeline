package pl.wp.opinions.pipeline.duplicates;

import com.google.common.hash.HashCode;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;
import pl.wp.opinions.domain.model.Post;

import java.util.Set;
import java.util.stream.Collectors;

@Log4j
@RequiredArgsConstructor
public class DuplicatesService {

    private final PostHashService postHashService;
    private final DuplicatesCache duplicatesCache;
    private final int hashCountThreshold;

    public Set<Post> getUniquePosts(final Set<Post> posts) {
        return posts.stream()
                .filter(this::isNotDuplicated)
                .collect(Collectors.toSet());
    }

    private boolean isNotDuplicated(final Post post) {
        return !isDuplicated(post);
    }

    public boolean isDuplicated(final Post post) {
        final HashCode hash = postHashService.getHash(post);
        return duplicatesCache.getCounterValue(hash) > hashCountThreshold;
    }
}
