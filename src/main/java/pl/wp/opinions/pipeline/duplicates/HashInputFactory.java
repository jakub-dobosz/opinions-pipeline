package pl.wp.opinions.pipeline.duplicates;

import pl.wp.opinions.domain.model.Post;

public class HashInputFactory {

    private static final int MAX_POST_TEXT_LENGTH = 30;

    private final int postTextLengthThreshold;

    public HashInputFactory(final int postTextLengthThreshold) {
        this.postTextLengthThreshold = postTextLengthThreshold;
    }

    public String getFromPost(Post post) {
        if (textIsTooShort(post.getText())) {
            return forTooShortText(post);
        } else {
            return forSufficientText(post);
        }
    }

    private boolean textIsTooShort(String postText) {
        return postText.length() < postTextLengthThreshold;
    }

    private String forTooShortText(Post post) {
        return post.getText() + post.getUserData().getUserName() + post.getUserData().getIpAddress();
    }

    private String forSufficientText(Post post) {
        final String postText = post.getText();
        return postText.length() > MAX_POST_TEXT_LENGTH ? postText.substring(0, MAX_POST_TEXT_LENGTH) : postText;
    }
}
