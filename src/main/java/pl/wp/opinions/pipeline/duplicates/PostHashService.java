package pl.wp.opinions.pipeline.duplicates;

import com.google.common.base.Charsets;
import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;
import lombok.RequiredArgsConstructor;
import pl.wp.opinions.domain.model.Post;

@RequiredArgsConstructor
public class PostHashService {

    private final HashInputFactory hashInputFactory;

    public HashCode getHash(Post post) {
        final String hashInput = hashInputFactory.getFromPost(post);
        return Hashing.farmHashFingerprint64().hashString(hashInput, Charsets.UTF_8);
    }
}
