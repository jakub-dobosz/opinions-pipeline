package pl.wp.opinions.pipeline.middleware;

import lombok.Cleanup;
import lombok.RequiredArgsConstructor;
import pl.wp.opinions.domain.model.Post;
import pl.wp.opinions.dto.BulkDto;
import pl.wp.opinions.dto.BulkDtoFactory;
import pl.wp.opinions.UnSuccessfulResponseException;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Set;

import static javax.ws.rs.core.Response.Status.Family.SUCCESSFUL;
import static javax.ws.rs.core.Response.Status.GONE;

@RequiredArgsConstructor
public class OpinionsMiddlewareService {

    private static final String ROUTE = "OpinionModule";

    private final WebTarget target;
    private final BulkDtoFactory bulkDtoFactory;
    private final DestinationPathProvider destinationPathProvider;

    public void send(Set<Post> posts) {
        BulkDto bulkDto = bulkDtoFactory.newInstance(posts);
        @Cleanup Response response = getResponse(bulkDto);

        if (isErroneous(response)) {
            throw new UnSuccessfulResponseException("CMS Message Broker zwrócił niepoprawny status : " +
                    response.getStatus() + ", message : " +
                    response.readEntity(String.class)
            );
        }
    }

    private Response getResponse(BulkDto bulkDto) {
        Post firstPost = bulkDto.getPosts().iterator().next();

        return target
                .path(destinationPathProvider.get(firstPost))
                .request(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .header("x-source", "OpinionsRestAPI")
                .header("x-destination", ROUTE)
                .post(Entity.json(bulkDto));
    }

    private boolean isErroneous(Response response) {
        return !response.getStatusInfo().getFamily().equals(SUCCESSFUL) && !response.getStatusInfo().equals(GONE);
    }
}