package pl.wp.opinions.pipeline.middleware;

import pl.wp.opinions.domain.model.Post;

public class DestinationPathProvider {

    private static final String PATH = "resources";

    public String get(Post post) {
        String path = getPath(post);
        return PATH + "/topic/" + path + "/_bulk";
    }

    private String getPath(Post post) {
        return post.getTopicContext() + "/" + post.getTopicObjectType() + "/" + post.getTopicObjectId();
    }
}
