package pl.wp.opinions.pipeline;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixThreadPoolProperties;
import lombok.extern.log4j.Log4j;
import pl.wp.opinions.configuration.HystrixConfiguration;
import pl.wp.opinions.configuration.logging.EventLog;
import pl.wp.opinions.domain.model.Post;

import java.util.Set;

@Log4j
public class OpinionsPipelineCommand extends HystrixCommand<Void> {

    private final OpinionsPipeline opinionsPipeline;
    private final EventLog eventLog;
    private final Set<Post> posts;

    public OpinionsPipelineCommand(final OpinionsPipeline opinionsPipeline,
                                   final HystrixConfiguration hystrixConfiguration,
                                   final EventLog eventLog,
                                   final Set<Post> posts) {
        super(Setter.withGroupKey(HystrixCommandGroupKey.Factory.asKey("OpinionsPipelineCommand"))
                .andThreadPoolPropertiesDefaults(HystrixThreadPoolProperties.Setter()
                        .withCoreSize(hystrixConfiguration.getCoreThreadPoolSize())
                        .withMaxQueueSize(hystrixConfiguration.getMaxQueueSize())));

        this.opinionsPipeline = opinionsPipeline;
        this.eventLog = eventLog;
        this.posts = posts;
    }

    @Override
    protected Void run() throws Exception {
        opinionsPipeline.process(posts)
                .doOnSuccess(eventLog::rejectedByES)
                .doOnSuccess(eventLog::created)
                .subscribe(this::logSuccess, this::logError);
        return null;
    }

    private void logSuccess(final Set<Post> posts) {
        log.debug("Posts send to middleware service: " + posts.toString());
    }

    private void logError(final Throwable cause) {
        log.error("Posts not processed in pipeline", cause);
    }
}