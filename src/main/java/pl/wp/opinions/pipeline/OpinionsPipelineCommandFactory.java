package pl.wp.opinions.pipeline;

import pl.wp.opinions.domain.model.Post;

import java.util.Set;

@FunctionalInterface
public interface OpinionsPipelineCommandFactory {

    OpinionsPipelineCommand create(final Set<Post> posts);
}
