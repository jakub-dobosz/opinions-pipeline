package pl.wp.opinions.pipeline;

import lombok.RequiredArgsConstructor;
import pl.wp.opinions.domain.model.Post;
import pl.wp.opinions.pipeline.gateway.OpinionsServicesGateway;
import pl.wp.opinions.pipeline.middleware.OpinionsMiddlewareService;
import rx.Observable;
import rx.Single;

import java.util.HashSet;
import java.util.Set;

@RequiredArgsConstructor
public class OpinionsPipeline {

    private final OpinionsServicesGateway gateway;
    private final OpinionsMiddlewareService middlewareService;

    /*TODO: ta metoda jest do zmiany, trzeba w inny sposob wysylac set postów do middleware, bo niepotrzebnie dwa razy subskrybuję się do Single:
    raz middlewareService, a drugi raz w OpinionsPipelineCommand, który wywołuje metodę OpinionsPipeline::process .*/
    Single<Set<Post>> process(final Set<Post> posts) {

        Single<Set<Post>> postsSingle = Observable.from(posts)
                .flatMap(this::postToObservable)
                .toList()
                .toSingle()
                .map(HashSet::new);

        postsSingle.subscribe(middlewareService::send);

        return postsSingle;
    }

    private Observable<Post> postToObservable(final Post post) {
        return gateway.getProcessedPost(post)
                .toObservable()
                .onExceptionResumeNext(Observable.empty());
    }
}