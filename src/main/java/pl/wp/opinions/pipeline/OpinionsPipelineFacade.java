package pl.wp.opinions.pipeline;

import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;
import com.google.common.collect.Sets;
import lombok.RequiredArgsConstructor;
import pl.wp.opinions.configuration.logging.EventLog;
import pl.wp.opinions.domain.model.Post;
import pl.wp.opinions.pipeline.duplicates.DuplicatesService;

import java.util.Set;

@RequiredArgsConstructor
public class OpinionsPipelineFacade implements RemovalListener<String, Set<Post>> {

    private final OpinionsPipelineCommandFactory commandFactory;
    private final DuplicatesService duplicatesService;
    private final EventLog eventLog;

    @Override
    public void onRemoval(RemovalNotification<String, Set<Post>> removalNotification) {
        process(removalNotification.getValue());
    }

    void process(final Set<Post> posts) {
        Set<Post> uniquePosts = duplicatesService.getUniquePosts(posts);
        Set<Post> duplicatedPosts = Sets.difference(posts, uniquePosts);
        eventLog.duplicated(duplicatedPosts);
        if (!uniquePosts.isEmpty()) commandFactory.create(uniquePosts).observe();
    }
}
