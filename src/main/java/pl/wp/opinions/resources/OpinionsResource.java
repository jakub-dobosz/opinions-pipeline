package pl.wp.opinions.resources;

import com.codahale.metrics.annotation.Timed;
import com.google.common.base.Throwables;
import com.google.common.util.concurrent.RateLimiter;
import lombok.extern.log4j.Log4j;
import pl.wp.opinions.router.AggregatedVotesRoute;
import pl.wp.opinions.router.ModeratorRoute;
import pl.wp.opinions.configuration.OpinionsConfiguration;
import pl.wp.opinions.configuration.logging.EventLog;
import pl.wp.opinions.domain.model.Post;
import rx.Observable;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static javax.ws.rs.core.Response.Status.CREATED;

/**
 * @author krabczak
 */
@Log4j
@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public class OpinionsResource {

    private final OpinionsCache opinionsCache;
    private final OpinionsConfiguration configuration;
    private final EventLog logger;

    private static final int VOTES_PAGE_SIZE = 200;

    private static final ConcurrentHashMap<String, RateLimiter> voteLimiters = new ConcurrentHashMap<>();
    private static final ConcurrentHashMap<String, RateLimiter> newOpinionsLimiters = new ConcurrentHashMap<>();
    private static final double LIMIT = 1;
    private static final ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
    private static final ScheduledExecutorService votesExecutor = Executors.newScheduledThreadPool(1);
    private static final ConcurrentHashMap<String, Long> votesAggregation = new ConcurrentHashMap<>();

    public OpinionsResource(OpinionsCache opinionsCache,
                            OpinionsConfiguration configuration) {
        this.opinionsCache = opinionsCache;
        this.configuration = configuration;
        this.logger = configuration.getEventsLogFactory().build();

        executor.scheduleAtFixedRate(voteLimiters::clear, 0, 24, TimeUnit.HOURS);
        executor.scheduleAtFixedRate(newOpinionsLimiters::clear, 0, 24, TimeUnit.HOURS);
        votesExecutor.scheduleAtFixedRate(() -> {
            final HashMap<String, Long> results = new HashMap<>();
            while (votesAggregation.keySet().size() > 0) {
                for (String key : votesAggregation.keySet()) {
                    if (results.size() >= VOTES_PAGE_SIZE) {
                        break;
                    }
                    results.put(key, votesAggregation.remove(key));
                }
                try {
                    if (results.size() > 0) {
                        new AggregatedVotesRoute(configuration, results)
                                .observe().toBlocking().toFuture().get(5000, TimeUnit.MILLISECONDS);
                    }
                } catch (Exception e) {
                    log.error("AggregatedVotesRoute failed.", e);
                } finally {
                    results.clear();
                }
            }
        }, 30, 30, TimeUnit.SECONDS);
    }

    @POST
    @Timed
    @Path("/topic/{contextKey}/{objectType}/{objectId}/thread")
    public Response createNewThread(@PathParam("contextKey") String contextKey,
                                    @PathParam("objectType") String objectType,
                                    @PathParam("objectId") String objectId,
                                    @Valid Post post,
                                    @Context UriInfo uriInfo) throws MessageNotQueuedException {
        post = setTopicData(post, contextKey, objectType, objectId);
        return produceOpinionThrottled(post, uriInfo);
    }

    @POST
    @Timed
    @Path("/topic/{contextKey}/{objectType}/{objectId}/thread/{threadId}")
    public Response createNewPost(@PathParam("contextKey") String contextKey,
                                  @PathParam("objectType") String objectType,
                                  @PathParam("objectId") String objectId,
                                  @PathParam("threadId") String threadId,
                                  @Valid Post post,
                                  @Context UriInfo uriInfo) throws MessageNotQueuedException {
        post = setTopicData(post, contextKey, objectType, objectId);
        post.setParentPath(threadId);
        return produceOpinionThrottled(post, uriInfo);
    }

    @POST
    @Timed
    @Path("/topic/{contextKey}/{objectType}/{objectId}/post/{postId}")
    public Response createAnswerToPost(@PathParam("contextKey") String contextKey,
                                       @PathParam("objectType") String objectType,
                                       @PathParam("objectId") String objectId,
                                       @PathParam("postId") String postId,
                                       @Valid Post answer,
                                       @Context UriInfo uriInfo) throws MessageNotQueuedException {
        answer = setTopicData(answer, contextKey, objectType, objectId);
        answer.setParentPath(postId);
        return produceOpinionThrottled(answer, uriInfo);
    }

    private Post setTopicData(Post post, String contextKey, String objectType, String objectId) {
        post.setTopicContext(contextKey);
        post.setTopicObjectId(objectId);
        post.setTopicObjectType(objectType);
        return post;
    }

    private Response produceOpinionThrottled(Post post, UriInfo uriInfo) throws MessageNotQueuedException {
        if (newOpinionsLimiters.computeIfAbsent(post.getUserData().getIpAddress(), s -> RateLimiter.create(LIMIT)).tryAcquire()) {
            return produceOpinions(post, uriInfo);
        } else {
            return Response.serverError().status(429).build();
        }
    }

    private Response produceOpinions(Post post, UriInfo uriInfo) throws MessageNotQueuedException {
        post.setDestinationPath(uriInfo.getPath());
        opinionsCache.put(post);
        return Response.status(CREATED).build();
    }

    @POST
    @Timed
    @Path("/opinion/{id}/upvote")
    public Response upVoteOpinion(@PathParam("id") String id, @Context UriInfo uriInfo) {
        if (voteLimiters.computeIfAbsent(id, s -> RateLimiter.create(LIMIT)).tryAcquire()) {
            votesAggregation.compute(id, (k, v) -> {
                final Long lastValue = v == null ? 0l : v;
                return lastValue + 1;
            });
            return Response.ok().build();
        } else {
            return Response.serverError().status(429).build();
        }
    }

    @POST
    @Timed
    @Path("/opinion/{id}/downvote")
    public Response downVoteOpinion(@PathParam("id") String id, @Context UriInfo uriInfo) {
        if (voteLimiters.computeIfAbsent(id, s -> RateLimiter.create(LIMIT)).tryAcquire()) {
            votesAggregation.compute(id, (k, v) -> {
                final Long lastValue = v == null ? 0l : v;
                return lastValue - 1;
            });
            return Response.ok().build();
        } else {
            return Response.serverError().status(429).build();
        }
    }

    @POST
    @Timed
    @Path("/opinion/{context}/{id}/report")
    public Response reportOpinion(@PathParam("context") String context, @PathParam("id") String id, @Context UriInfo uriInfo,
                                  @Context HttpServletRequest req) {
        logHeaders(req);
        sendRequest(new ModeratorRoute(configuration, id, removeContext(context, uriInfo.getPath())).observe()
                .doOnCompleted(() -> logger.reported(id, context)));
        return Response.ok().build();
    }

    private void logHeaders(HttpServletRequest req) {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("remoteAddr: ").append(req.getRemoteAddr()).append(", Headers: ");
        final Enumeration<String> headerNames = req.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            final String headerName = headerNames.nextElement();
            final String header = req.getHeader(headerName);
            stringBuilder.append(headerName).append(": ").append(header).append(", ");
        }
        log.info(stringBuilder.toString());
    }

    private String removeContext(String context, String path) {
        int startIndex = path.indexOf(context);
        final String toBeReplaced = path.substring(startIndex, startIndex + context.length() + 1);
        return path.replace(toBeReplaced, "");
    }

    private void sendRequest(Observable<Void> command) {
        try {
            command.toBlocking().toFuture().get(1500, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | TimeoutException | ExecutionException e) {
            Throwables.propagate(e);
        }
    }
}
