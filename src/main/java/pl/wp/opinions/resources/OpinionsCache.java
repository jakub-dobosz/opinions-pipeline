package pl.wp.opinions.resources;

import pl.wp.opinions.domain.model.Post;

@FunctionalInterface
public interface OpinionsCache {

    void put(final Post post);
}
