package pl.wp.opinions.resources;

public class MessageNotQueuedException extends Exception {
    public static final long serialVersionUID = 1L;

    public MessageNotQueuedException() {
        super("Nie udalo się zapisac opinii do kolejki");
    }
}
