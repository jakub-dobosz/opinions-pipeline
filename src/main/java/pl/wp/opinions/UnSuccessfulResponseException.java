package pl.wp.opinions;

public class UnSuccessfulResponseException extends RuntimeException {
    public static final long serialVersionUID = 1L;

    public UnSuccessfulResponseException(String message) {
        super(message);
    }

    public UnSuccessfulResponseException(String message, Exception exception) {
        super(message, exception);
    }
}
